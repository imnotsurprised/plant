<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'modules' => [
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableFlashMessages' => false,
            'enableRegistration' => false,
            'enableGeneratingPassword' => false,
            'enableConfirmation' => false,
            'enableUnconfirmedLogin' => true,
            'enablePasswordRecovery' => false,
            'modelMap' => [
                'User' => 'common\models\User'
            ],
            'controllerMap' => [
                'admin' => 'backend\controllers\AdminController'
            ]
        ],
        'rbac' => 'dektrium\rbac\RbacWebModule'
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'dektrium\rbac\components\DbManager',
            'defaultRoles' => ['admin'],
        ]
    ],
];

<?php
use yii\helpers\Html;

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/registration', 'token' => $token]);
?>
<div class="password-reset">
    <p>Hello dear user,</p>

    <p>Follow the link below to your registration:</p>

    <p><?= Html::a(Html::encode($resetLink), $resetLink) ?></p>
</div>
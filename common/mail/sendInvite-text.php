<?php

/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->urlManager->createAbsoluteUrl(['site/registration', 'token' => $token]);
?>
Hello dear user,

Follow the link below to your registration:

<?= $resetLink ?>

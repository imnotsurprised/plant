<?php
namespace common\models;

use yii;
use dektrium\user\models\User as BaseUser;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $first_name
 * @property string $last_name
 * @property string $gender
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 */
class User extends BaseUser
{
    const GENDERS = ['male', 'female'];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();

        $scenarios['create']   = ['first_name', 'last_name', 'gender'];
        $scenarios['update']   = ['first_name', 'last_name', 'gender'];
        $scenarios['register'] = ['first_name', 'last_name', 'gender'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();

        $rules['fieldRequired'] = [['first_name', 'last_name', 'gender'], 'required'];

        return $rules;
    }

   public function getIsAdmin()
   {
       return Yii::$app->user->can('manage_roles');
   }

   public function getRole()
   {
       return $this->hasOne(AuthAssignment::className(), ['user_id' => 'id']);
   }

    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }
}

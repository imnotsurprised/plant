<?php

namespace backend\controllers;

use yii;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use dektrium\user\filters\AccessRule;

use dektrium\user\controllers\AdminController as BaseAdminController;
use common\models\User;

class AdminController extends BaseAdminController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['update', 'delete', 'assignments'],
                        'allow' => true,
                        'roles' => [
                            'admin',
                            'head_egineering_department',
                            'deputy_head_egineering_department'
                        ]
                    ]
                ],
            ],
        ];
    }

    public function actionUpdate($id)
    {
        $user = $this->findModel($id);
        $this->checkAvailableModify($user);

        $user->scenario = 'update';
        $event = $this->getUserEvent($user);

        $this->performAjaxValidation($user);

        $this->trigger(self::EVENT_BEFORE_UPDATE, $event);

        if ($user->load(\Yii::$app->request->post()) && $user->save()) {
            \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'Account details have been updated'));
            $this->trigger(self::EVENT_AFTER_UPDATE, $event);
            return $this->refresh();
        }

        return $this->render('_account', [
            'user' => $user,
            'genders' => User::GENDERS
        ]);
    }

    public function actionDelete($id)
    {
        $user = $this->findModel($id);
        $this->checkAvailableModify($user);

        return parent::actionDelete($id);
    }

    private function checkAvailableModify($modifyUser)
    {
        if ($modifyUser->id == Yii::$app->user->identity->id)
        {
            return true;
        }

        $currentRole = Yii::$app->user->identity->role;
        $childRoles = Yii::$app->authManager->getChildRoles($currentRole->item_name);

        if (!empty($childRoles))
        {
            foreach ($childRoles as $childRole)
            {
                if ($childRole->name == $modifyUser->role->item_name)
                {
                    return true;
                }
            }
        }

        throw new ForbiddenHttpException('You are not allowed to perform this action.');
    }
}
<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\filters\AccessControl;
use dektrium\user\filters\AccessRule;
use yii\helpers\ArrayHelper;

use backend\models\InviteForm;

class InviteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => [
                            'admin',
                            'head_egineering_department',
                            'deputy_head_egineering_department'
                        ]
                    ]
                ]
            ]
        ];
    }

    public function actionIndex()
    {
        $roles = ArrayHelper::map(Yii::$app->authManager->getRoles(), 'name', 'description');

        $inviteForm = new InviteForm();
        $inviteForm->token = Yii::$app->security->generateRandomString(15);

        if ($inviteForm->load(Yii::$app->request->post()) && $inviteForm->validate())
        {
            $request = Yii::$app->request->post('InviteForm');
            $currentRole = Yii::$app->user->identity->role;

            $childRoles = Yii::$app->authManager->getChildRoles($currentRole->item_name);

            $inviteChildRole = false;

            if (!empty($childRoles))
            {
                foreach ($childRoles as $childRole)
                {
                    if ($childRole->name == $request['role'] && $childRole->name !== $currentRole->item_name)
                    {
                        $inviteChildRole = true;
                    }
                }
            }

            if ($inviteChildRole)
            {
                if ($inviteForm->invite())
                {
                    if ($inviteForm->sendEmail())
                    {
                        Yii::$app->session->setFlash('success', 'Invite send successfully.');
                    }
                    else
                    {
                        Yii::$app->session->setFlash('error', 'Error from send invite');
                    }
                }
            }
            else
            {
                throw new ForbiddenHttpException('You are not allowed to invite user for this role.');
            }

            return $this->redirect('/admin/user/admin');
        }

        return $this->render('invite-form', [
            'roles' => $roles,
            'model' => $inviteForm
        ]);
    }
}
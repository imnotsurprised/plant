<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use dektrium\user\filters\AccessRule;

use common\models\User;

use backend\models\Invite;
use backend\models\RegistrationForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['registration', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionRegistration($token)
    {
        $inviteData = Invite::findOne(['token' => $token]);

        if (empty($inviteData))
        {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

        $registrationForm = new RegistrationForm();

        $registrationForm->email = $inviteData->email;
        $registrationForm->role = $inviteData->role;
        $registrationForm->roleDescription = Yii::$app->authManager->getRole($inviteData->role);
        $registrationForm->roleDescription = $registrationForm->roleDescription->description;

        if ($registrationForm->load(Yii::$app->request->post()))
        {
            if ($user = $registrationForm->signup())
            {
                if (Yii::$app->getUser()->login($user))
                {
                    return $this->goHome();
                }
            }
        }

        return $this->render('registration-form', [
            'model' => $registrationForm,
            'genders' => User::GENDERS
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}

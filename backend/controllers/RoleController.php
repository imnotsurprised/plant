<?php

namespace backend\controllers;

use yii;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\BadRequestHttpException;
use dektrium\user\filters\AccessRule;

use yii\rbac\Role;
use yii\rbac\Item;

use dektrium\rbac\controllers\ItemControllerAbstract as AbstractController;

class RoleController extends AbstractController
{
    /** @var string */
    protected $modelClass = 'dektrium\rbac\models\Role';

    protected $type = Item::TYPE_ROLE;

    /** @inheritdoc */
    protected function getItem($name)
    {
        $role = \Yii::$app->authManager->getRole($name);

        if ($role instanceof Role) {
            return $role;
        }

        throw new NotFoundHttpException;
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
                'rules' => [
                    [
                        'actions' => ['index', 'update', 'delete'],
                        'allow' => true,
                        'roles' => [
                            'admin',
                            'head_egineering_department',
                            'deputy_head_egineering_department'
                        ]
                    ]
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return parent::actionIndex();
    }

    public function actionUpdate($name)
    {
        $this->checkAvailableUpdate($name);

        return parent::actionUpdate($name);
    }

    private function checkAvailableUpdate($name)
    {
        $currentRole = Yii::$app->user->identity->role;
        $childRoles = Yii::$app->authManager->getChildRoles($currentRole->item_name);

        if (!empty($childRoles))
        {
            foreach ($childRoles as $childRole)
            {
                if ($childRole->name == $name && $childRole->name !== $currentRole->item_name)
                {
                    return true;
                }
            }
        }

        throw new ForbiddenHttpException('You are not allowed to edit this role.');
    }

    public function actionDelete($name)
    {
        $this->checkAvailableDelete($name);
    }

    private function checkAvailableDelete($name)
    {
        $users = Yii::$app->authManager->getUserIdsByRole($name);

        if (!empty($users))
        {
            throw new BadRequestHttpException('This role assigned to user.');
        }

        $currentRole = Yii::$app->user->identity->role;
        $childRoles = Yii::$app->authManager->getChildRoles($currentRole->item_name);

        if (!empty($childRoles))
        {
            foreach ($childRoles as $childRole)
            {
                if ($childRole->name == $name && $childRole->name !== $currentRole->item_name)
                {
                    return true;
                }
            }
        }

        throw new ForbiddenHttpException('You are not allowed to remove this role.');
    }
}
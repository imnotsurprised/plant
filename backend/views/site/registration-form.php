<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\widgets\Select2;

$this->title = 'Registration';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="registration-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput() ?>

    <?= $form->field($model, 'first_name')->textInput() ?>

    <?= $form->field($model, 'last_name')->textInput() ?>

    <?= $form->field($model, 'gender')->widget(Select2::className(), [
        'data' => $genders,
        'options' => [
            'placeholder' => 'Please specify gender'
        ],
    ]) ?>

    <?= $form->field($model, 'roleDescription')->textInput(['readonly' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Registration', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

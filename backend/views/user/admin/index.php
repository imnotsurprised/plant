<?php

use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = Yii::t('user', 'Manage users');
$this->params['breadcrumbs'][] = $this->title;

?>

<?= $this->render('/_alert', ['module' => Yii::$app->getModule('user')]) ?>

<?php Pjax::begin() ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel'  => $searchModel,
    'layout'       => "{items}\n{pager}",
    'columns' => [
        'first_name',
        'last_name',
        'email:email',
        [
            'attribute' => 'role',
            'format' => 'raw',
            'value' => function($model) {
                $role = Yii::$app->authManager->getRole($model->role->item_name);
                if (!empty($role)) {
                    return $role->description;
                }
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}'
        ]
    ]
]); ?>

<?php Pjax::end() ?>
<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use kartik\widgets\Select2;

?>

<?php $this->beginContent('@dektrium/user/views/admin/update.php', ['user' => $user]) ?>

<?php $form = ActiveForm::begin([
    'layout' => 'horizontal',
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'wrapper' => 'col-sm-9',
        ],
    ],
]); ?>

<?= $form->field($user, 'email')->textInput(['maxlength' => 255]) ?>

<?= $form->field($user, 'username')->textInput(['maxlength' => 255]) ?>

<?= $form->field($user, 'password')->passwordInput() ?>

<?= $form->field($user, 'first_name')->textInput(['maxlength' => 255]) ?>

<?= $form->field($user, 'last_name')->textInput(['maxlength' => 255]) ?>

<?= $form->field($user, 'gender')->widget(Select2::className(), [
    'data' => $genders,
    'options' => [
        'placeholder' => 'Please specify gender'
    ],
]) ?>

<?= Html::submitButton( 'Update', ['class' => 'btn btn-success btn-block']) ?>

<?php ActiveForm::end(); ?>

<?php $this->endContent() ?>

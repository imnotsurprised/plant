<?php

namespace backend\models;

use yii;
use yii\base\Model;

class InviteForm extends Model
{
    public $email;
    public $token;
    public $role;

    public function rules()
    {
        return [
            [['email', 'token', 'role'], 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['token', 'string', 'max' => 15]
        ];
    }

    public function invite()
    {
        $invite = new Invite();
        $invite->email = $this->email;
        $invite->role = $this->role;
        $invite->token = $this->token;
        return $invite->save() ? true : null;
    }

    public function sendEmail()
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'sendInvite-html', 'text' => 'sendInvite-text'],
                ['token' => $this->token]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('send registration invite for ' . Yii::$app->name)
            ->send();
    }
}
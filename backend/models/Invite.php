<?php

namespace backend\models;

use yii\db\ActiveRecord;

class Invite extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%invites}}';
    }
}
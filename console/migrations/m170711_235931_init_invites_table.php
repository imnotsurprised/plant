<?php

use yii\db\Migration;

class m170711_235931_init_invites_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%invites}}', [
            'id' => $this->primaryKey(),
            'email' => $this->string()->notNull(),
            'role' => $this->string(50)->notNull(),
            'token' => $this->string(15)->notNull(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%invites}}');
    }
}
